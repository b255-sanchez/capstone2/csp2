const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
			
			email : {
				type : String,
				required : [true, "Email is required"]
			},
			password : {
				type : String,
				required : [true, "Password is required"]
			},
			isAdmin : {
				type : Boolean,
				default : false
			},
			
			orderedProduct : 
			[
				{
					products : [
					{

						productId : {
						type : String,
						required : [true, "product ID is required"]
						},

						productName : {
						type : String,
						required : [true, "product name is required"]
						},

						quantity : {
						type : String,
						required : [true, "quantity is required"]
						}
					}
					],

					totalAmount : {
						type : String, 
						required : [true, "Mobile No is required"]
					},
					
					purchasedOn : {
						type : Date,
						default : new Date()
					}
				}
			]
		})

module.exports = mongoose.model("User", userSchema);