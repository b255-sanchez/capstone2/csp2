
const User = require("../models/User");
const Product = require("../models/Product")
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		} else {
			return false;
		}
	})
};

module.exports.registerUser = (reqBody) =>{

	let newUser = new User({

		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)

	})


	return newUser.save().then((user, error) =>{

		if(error) {
			return false;
		} else {
			return true
		};
	})


}

module.exports.loginUser = (reqBody) => {

	return User.findOne({email : reqBody.email}).then(result => {

		if(result == null){
			return false;

		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)


			if(isPasswordCorrect) {

				return {access : auth.createAccessToken(result)}
	
			} else {
				return false;
			}
		}
	})
}

module.exports.getProfile = (userId) => {
  return User.findOne({ _id: userId })
    .then(result => {
      if (result == null) {
        return false;
      } else {
        const { _id, isAdmin } = result; // Destructure the necessary fields
        return { _id, isAdmin }; // Return the user ID and isAdmin status
      }
    });
};



module.exports.order = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {
		


		return user.save().then((user, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		})
	})


	let isProductUpdated = await Product.findById(data.productId).then(product =>{

		product.userOrders.push({userId : data.userId});

		return product.save().then((product, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		})
	})


	if(isUserUpdated && isProductUpdated){
		return true;

	} else {
		return false
	}
};