const Product = require("../models/Product");


module.exports.addProduct = (reqBody) => {
	let newProduct = new Product({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price,
		profilePictureURL : reqBody.profilePictureURL
	});


	return newProduct.save().then((product, error) => {

		if(error) {
			return false;

	
		} else {
			return product
		}
	})
}


module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}


module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
}


module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result
	});
}


module.exports.updateProduct = (reqParams, reqBody) => {


	let updateProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	}

	return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, error) =>{

		if(error){
			return false;

		}else{
			return product;
		}
	})
}

module.exports.toggleProductAvailability = (req, res) => {
  const { productId } = req.params;
  const { isAvailable } = req.body;

  // Update the product's availability in the database
  Product.findByIdAndUpdate(
    productId,
    { isAvailable },
    { new: true }
  )
    .then((updatedProduct) => {
      res.json(updatedProduct);
    })
    .catch((error) => {
      console.log(error);
      res.status(500).json({ error: 'An error occurred while updating the product availability.' });
    });
};

module.exports.archiveProduct = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {


		if (error) {

			return false;


		} else {

			return true;

		}

	});
};
