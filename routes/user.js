const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");


router.post("/checkEmail", (req, res) => {
  userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});


router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});


router.get("/details", auth.verify, (req, res) => {
  const userId = auth.decode(req.headers.authorization).id // Access the user ID from the decoded token

  userController.getProfile(userId).then(resultFromController => res.send(resultFromController));
});




router.post("/order", auth.verify, (req,res) =>{
  let data = {
    userId: auth.decode(req.headers.authorization).id,
    productId: req.body.productId
  }

  userController.order(data).then(resultFromController => res.send (resultFromController));
})


module.exports = router;
